// console.log("Hello World")

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};


console.log(trainer);
console.log("Result of dot notation");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){
	    console.log(this.name + " tackled " + target.name);
	    
	    target.health -= this.attack
	    console.log(target.name + " health is now reduced to "+target.health);

	    if(target.health <= 0){
	        target.faint();
	    }
	}
	this.faint = function(){
	    console.log(this.name + " fainted ");
	}
}

let Pikachu = new Pokemon("Pikachu", 12);
let Geodude = new Pokemon("Geodude", 8);
let Mewtwo = new Pokemon("Mewtwo", 100);

Geodude.tackle(Pikachu);
console.log(Pikachu);
Mewtwo.tackle(Geodude);
console.log(Geodude);